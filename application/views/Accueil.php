<?php 
$page = 1;
if(isset($_GET['page'])){
    if($_GET['page']>=1 AND $_GET['page']<=17){
        $page = $_GET['page'];
    }
}?>

<ul class="pagination">
    <?php 
    $ppp=10;//PokePerPage
    if($nb%$ppp!=0){
        $nb+=$ppp;
    }
    for ($i=1.0;$i<=$nb/($ppp*1.0);$i++):
        if($i!=$page):
            if(isset($filtre)):?>
                <li class="waves-effect"><a href="<?php echo site_url("filtre/".$filtre."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php elseif(isset($_GET['filtre'])):?>
                <li class="waves-effect"><a href="<?php echo site_url("filtre/".$_GET['filtre']."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php else:?>
                <li class="waves-effect"><a href="<?php echo site_url("accueil?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php endif;
        else:
            if(isset($filtre)):?>
                <li class="active"><a href="<?php echo site_url("filtre/".$filtre."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php elseif(isset($_GET['filtre'])):?>
                <li class="active"><a href="<?php echo site_url("filtre/".$_GET['filtre']."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php else:?>
                <li class="active"><a href="<?php echo site_url("accueil?page=".$i);?>">
            <?php echo $i;?></a></li>
            <?php endif;?>
        <?php endif;?>
    <?php endfor;?>
</ul>

<table border=1>
    <thead>
        <th><a href="<?php echo site_url("accueil");?>">Pokémon</a></th>
        <th><a href="<?php echo site_url("accueil");?>">Numéro</a></th>
        <th><a href="<?php echo site_url("filtre/identifier");?>">Nom</a></th>
        <th><a href="<?php echo site_url("filtre/height");?>">Taille</a></th>
        <th><a href="<?php echo site_url("filtre/weight");?>">Poids</a></th>
        <th><a href="<?php echo site_url("filtre/experience");?>">Points de compétences</a></th>
    </thead>

    <?php
    $count = 1;
    foreach($pokedex as $pokemon):
        if(($count>($page-1)*$ppp) AND ($count<=($page*$ppp))):?>
            <tr>
                <td>
                    <img class="img_pokemon" src="<?php echo base_url("pokemon_artworks/".$pokemon['pokemon_id'].".png")?>" alt="img<?php echo $pokemon['pokemon_id']?>">
                </td>
                <td><p><?php echo $pokemon['pokemon_id']?></p></td>
                <td><p><?php echo $pokemon['identifier']?></p></td>
                <td><p><?php echo $pokemon['height']?></p></td>
                <td><p><?php echo $pokemon['weight']?></p></td>
                <td><p><?php echo $pokemon['base_experience']?></p></td>
                <td>
                    <?php
                    $present = false;
                    if(isset($collect)){
                        foreach($collect as $pokemo){
                            if($pokemo['pokemon_id']==$pokemon['pokemon_id']){
                                $present = true;
                            }
                        }
                    }
                    if((isset($_SESSION['login'])) AND ($this->session->userdata('login')!='admin') AND ($pokemon['pokemon_id'] != 808) AND ($pokemon['pokemon_id'] != 809) AND $present == false):
                        if(isset($filtre)):?>
                            <a href="<?php echo site_url("filtre/add_collect?page=".$page."&id=".$pokemon['pokemon_id'])."&filtre=".$filtre.'"'?>" class="btn-floating btn-large waves-effect waves-light red">
                                <img class="ajouter" src="<?php echo base_url("image/poke_add.jpg");?>" alt="Ajouter" title="Ajouter">
                            </a>
                        <?php elseif(isset($_GET['filtre'])):?>
                            <a href="<?php echo base_url("index.php/accueil/add_collect?page=".$page."&id=".$pokemon['pokemon_id'])."&filtre=".$_GET['filtre'].'"'?>" class="btn-floating btn-large waves-effect waves-light red">
                                <img class="ajouter" src="<?php echo base_url("image/poke_add.jpg");?>" alt="Ajouter" title="Ajouter">
                            </a>
                        <?php else:?>
                            <a href="<?php echo base_url("index.php/accueil/add_collect?page=".$page."&id=".$pokemon['pokemon_id']).'"'?>" class="btn-floating btn-large waves-effect waves-light red">
                                <img class="ajouter" src="<?php echo base_url("image/poke_add.jpg");?>" alt="Ajouter" title="Ajouter">
                            </a>
                        <?php endif;
                    endif;?>
                </td>
            </tr>
        <?php endif;
        $count++;
    endforeach?>
</table>

<ul class="pagination">
    <?php
    for ($i=1.0;$i<=$nb/($ppp*1.0);$i++):
        if($i!=$page):
            if(isset($filtre)):?>
                <li class="waves-effect"><a href="<?php echo site_url("filtre/".$filtre."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php elseif(isset($_GET['filtre'])):?>
                <li class="waves-effect"><a href="<?php echo site_url("filtre/".$_GET['filtre']."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php else:?>
                <li class="waves-effect"><a href="<?php echo site_url("accueil?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php endif;
        else:
            if(isset($filtre)):?>
                <li class="active"><a href="<?php echo site_url("filtre/".$filtre."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php elseif(isset($_GET['filtre'])):?>
                <li class="active"><a href="<?php echo site_url("filtre/".$_GET['filtre']."?page=".$i);?>">
                    <?php echo $i;?></a></li>
            <?php else:?>
                <li class="active"><a href="<?php echo site_url("accueil?page=".$i);?>">
            <?php echo $i;?></a></li>
            <?php endif;?>
        <?php endif;?>
    <?php endfor;?>
</ul>

<?php
if(isset($_SESSION['login']) AND $_SESSION['login']!='admin'):
    $count = 0;
    $collect = $this->pokemon_model->pokedex_get_collect();
    foreach($collect as $pokemon){
        $count = $count + 1;
    }
    if($count==0):
        //
    else:?>
        <div class="mini_collection fixed-action-btn">
            <a class="btn-floating btn-large red">
                <i class="large material-icons"><img class="ajouter" src="<?php echo base_url("image/poke_col.jpg")?>"> </i> </a> <ul>
                    <?php foreach($collect as $pokemon):?>
                    <li><a class="btn-floating white">
                            <img class="disabled" src="<?php echo base_url("pokemon_miniatures/".$pokemon['pokemon_id'].".png")?>" alt="img<?php echo $pokemon['pokemon_id']?>">
                        </a></li>
                    <?php endforeach;?>
                    </ul>
        </div>
    <?php endif;
endif;?>

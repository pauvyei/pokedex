<nav>
    <div class="nav-wrapper red pokedex">
        <a href="<?php echo site_url("accueil");?>" class="brand-logo"><img src="<?php echo base_url("image/logo.png");?>" alt="Pokédex"  width="280px"></a>
        <ul class="right">
            <?php if(isset($_SESSION['login'])):?> <!-- Utilisateur connecté -->
                <?php if($this->session->userdata('login')!='admin'):?> <!-- & non admin (membre) -->
                    <li><?php echo $this->session->userdata('login');?></li>
                    <li><a href="<?php echo site_url("collection");?>">Collection</a></li>
                    <li><a href="<?php echo site_url("logout");?>">Déconnexion</a></li>
                <?php elseif($this->session->userdata('login')=='admin'):?> <!-- & admin (non membre) -->
                    <li><?php echo $this->session->userdata('login');?></li>
                    <li><a href="<?php echo site_url("administration");?>">Panneau d'administration</a></li>
                    <li><a href="<?php echo site_url("logout");?>">Deconnexion</a></li>
                <?php endif?>
            <?php else:?> <!-- Non connecté | Visiteur -->
                <li><a href="<?php echo site_url("signup");?>">Inscription</a></li>
                <li><a href="<?php echo site_url("login");?>">Connexion</a></li>
            <?php endif?>
        </ul>
    </div>
</nav>
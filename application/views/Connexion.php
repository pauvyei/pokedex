<h2>Connexion</h2>
<?php echo validation_errors();?>
<?php echo form_open('login/connexion_valide')?>
    <div class="input-field col s6">
        <label for="login">Identifiant</label>
        <input id="login" name="login" type="text" class="validate">
    </div>
    <div class="input-field col s6">
        <label for="password">Mot de passe</label>
        <input id="password" name="password" type="password" class="validate">
    </div>

    <button class="btn waves-effect waves-light pulse red" type="submit" name="action">
        Connexion
        <!--<i class="material-icons right">send</i>-->
    </button>
</form>
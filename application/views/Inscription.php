<?php
if(!isset($_SESSION['login'])):?> <!--Si aucun utilisateur est connecté -->
    <h2>Inscription</h2>
    <?php echo validation_errors();?>
    <?php echo form_open('signup/inscription_valide');?>
        <div class="row">
            <div class="input-field col s6">
                <label for="firstname">Nom</label>
                <input id="firstname" name="firstname" type="text" class="validate">
            </div>
            <div class="input-field col s6">
                <label for="name">Prénom</label>
                <input id="name" name="name" type="text" class="validate">
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <label for="login">Identifiant</label>
                <input id="login" name="login" type="text" class="validate">
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <label for="password">Mot de passe</label>
                <input id="password" name="password" type="password" class="validate"/><br/>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <label for="passconf">Confirmer Mot de passe</label>
                <input id="passconf" name="passconf" type="password" class="validate"/><br/>
            </div>
        </div>

        <button class="btn waves-effect waves-light pulse red" type="submit" name="action">
            S'inscrire
            <!--<i class="material-icons right">send</i>-->
        </button>
    </form>
<?php else:
    echo 'Accès non-autorisé aux membres connectés.';
endif;?>
<?php
$count = 0;
$collect = $this->pokemon_model->pokedex_get_collect();
foreach($collect as $pokemon){
    $count = $count + 1;
}
if($count==0):?>
    <p>Votre collection est vide. Allez dans le pokédex et cliquez sur la pokéball pour ajouter un pokémon.</p>
<?php else:?>
    <table border=1>
        <thead>
            <th><p>Pokémon</p></th>
            <th><p>Numéro</p></th>
            <th><p>Nom</p></th>
            <th><p>Taille</p></th>
            <th><p>Poids</p></th>
            <th><p>Points de compétence</p></th>
        </thead>
            <!-- Liste la collection de l'utilisateur connecté (non admin) -->
            <?php foreach($collect as $pokemon):?>
            <tr>
                <td>
                    <img class="img_pokemon" src="<?php echo base_url("pokemon_artworks/".$pokemon['pokemon_id'].".png")?>" alt="img<?php echo $pokemon['pokemon_id']?>">
                </td>
                <td><p><?php echo $pokemon['pokemon_id']?></p></td>
                <td><p><?php echo $pokemon['identifier']?></p></td>
                <td><p><?php echo $pokemon['height']?></p></td>
                <td><p><?php echo $pokemon['weight']?></p></td>
                <td><p><?php echo $pokemon['base_experience']?></p></td>

                <td><?php if(isset($_SESSION['login']) AND $_SESSION['login']!='admin'):?>
                    <a href="<?php echo base_url("index.php/collection/del_collect?id=".$pokemon['pokemon_id']).'"'?>" class="btn-floating btn-large waves-effect waves-light red">
                        <img class="ajouter" src="<?php echo base_url("image/poke_del.jpg");?>" alt="Supprimer" title="Supprimer">
                    </a>
                    <?php endif?>
                </td>
            </tr>
            <?php endforeach?>
    </table>
<?php endif;?>
<?php
if($this->session->userdata('login')=='admin'):?> <!--//Si admin est bien connecté-->
    <table border=1>
        <thead>
            <th><p>Prénom</p></th>
            <th><p>Nom</p></th>
            <th><p>Login</p></th>
        </thead>
        <!-- Liste les utilisateurs -->
        <?php foreach($users as $collector):?>
            <tr>
                <?php if($collector['login']!='admin'):?> 
                    <td><p><?php echo $collector['name']?></p></td>
                    <td><p><?php echo $collector['firstname']?></p></td>
                    <td><p><?php echo $collector['login']?></p></td>
                    <td>
                        <?php if($this->session->userdata('login')=='admin'):?> <!-- Sécurité : Vérifie que l'utilisateur est bien admin pour pouvoir bannir un membre-->
                            <a href="<?php echo base_url("index.php/administration/del_user?login=".$collector['login']).'"'?>" class="btn waves-effect waves-light red" onclick="return confirm('Voulez-vous vraiment bannir <?php echo $collector['login']?> ?');">BANNIR</a>
                        <?php endif?>

                    </td>
                <?php endif?>

            </tr>
        <?php endforeach?>
    </table>
<?php else:
    echo 'Accès non-autorisé aux membres non administrateur.';
endif;
?>
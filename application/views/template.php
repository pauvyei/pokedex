<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
    <head>
        <meta charset = "utf-8" />
        <title><?php echo $content;?> - Pokedex</title>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("style.css");?>">
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var elems = document.querySelectorAll('.fixed-action-btn');
                var instances = M.FloatingActionButton.init(elems, {
                  direction: 'bottom',
                  hoverEnabled: false
                
                });
              });
        </script>
    </head>
    <body>
        <div id="global">
            <header>
                <!-- Les "nav" varies en fonction de l'état de la session -->
                <!-- Non connecté | Visiteur -->
                <!-- Connecté en tant que membre -->
                <!-- Connecté en tant qu'admin -->
                <?php $this->load->view("nav");?>
            </header> <!-- #entete -->
            
            <div id="contenu">
                <?php $this->load->view($content);?>
            </div> <!-- #contenu -->
            <footer class="red">
                <strong>&copy;2018 PAUVY Enzo | &copy;2018 Pokemon. TM</strong>
            </footer> <!-- #pied -->
        </div> <!-- #global -->
    </body>
</html>
<?php
class User_model extends CI_Model{
    protected $table='_collector';
    public function __construct(){
        $this->load->database();
    }
    //inscrit un utilisateur
    public function user_signup($login,$name,$firstname,$password){
        //Inscrit l'utilisateur
        $data=array('login'     => $login,
                    'name'      => $name,
                    'firstname' => $firstname,
                    'password'  => $password);
        return $this->db->insert('_collector',$data);
    }
    //Connexion de l'utilisateur
    public function user_login($login,$password){
        //Connecte l'utilisateur
        return $this->db->select('login,password')
                    ->from($this->table)
                    ->where('login',$login)
                    ->where('password',$password)
                    ->get()
                    ->result();
    }
    //récupérer un utilisateur
    public function get_user($login) {
		$this->db->from('_collector');
		$this->db->where('login',$login);
		return $this->db->get()->row();
	}
}
?>
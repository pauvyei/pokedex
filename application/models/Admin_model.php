<?php
class Admin_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
    //fonction permettant
    public function users_get_collector(){
        //Liste les users
        $query=$this->db->get('_collector');
        return $query->result_array();
    }
    
    //supprimer un utilisateur en tant qu'admin
    public function del_user($login){
        //Si un admin est connecté
        if(isset($_SESSION['login']) AND $_SESSION['login']=='admin'){
            $data = array('collector_login' => $login);
            $this->db->delete('_collect', $data);
            $data = array('login' => $login);
            $this->db->delete('_collector', $data);
        }
    }
}
?>

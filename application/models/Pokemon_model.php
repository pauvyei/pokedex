<?php
class Pokemon_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
    //récupérer la liste des pokémon de manière ascendante triée par ID
    public function pokedex_get_pokemon(){
        
        $query=$this->db->order_by('pokemon_id', 'asc')->get('_pokemon');
        return $query->result_array();
    }
    //récupérer la liste des pokémon de manière ascendante triée par nom
    public function pokedex_get_pokemon_identifier(){
        
        $query=$this->db->order_by('identifier', 'asc')->get('_pokemon');
        return $query->result_array();
    }
    //récupérer la liste des pokémon de manière ascendante triée par taille
    public function pokedex_get_pokemon_height(){
        
        $query=$this->db->order_by('height', 'asc')->get('_pokemon');
        return $query->result_array();
    }
    //récupérer la liste des pokémon de manière ascendante triée par poids
    public function pokedex_get_pokemon_weight(){
        
        $query=$this->db->order_by('weight', 'asc')->get('_pokemon');
        return $query->result_array();
    }
    //récupérer la liste des pokémon de manière ascendante triée par base_experience
    public function pokedex_get_pokemon_experience(){
        
        $query=$this->db->order_by('base_experience', 'asc')->get('_pokemon');
        return $query->result_array();
    }
    //récupérer la collection de l'utilisateur connecté
    public function pokedex_get_collect(){
        $query=$this->db->join('_pokemon', '_pokemon.pokemon_id = _collect.pokemon_id')->order_by('base_experience', 'desc')->get_where('_collect', array('collector_login' => $this->session->userdata('login')));
        
        return $query->result_array();
    }
    //récupérer la collection de l'utilisateur connecté, avec les miniatures dans la masterball
    public function pokedex_get_collect_mini(){
        $query=$this->db->join('_pokemon', '_pokemon.pokemon_id = _collect.pokemon_id')->order_by('base_experience', 'asc')->get_where('_collect', array('collector_login' => $this->session->userdata('login')));
        
        return $query->result_array();
    }
    //ajouter un pokémon à la collection
    public function pokedex_add_pokemon($pokemon_id){    
        $data = array('pokemon_id' => $pokemon_id, 'collector_login' => $this->session->userdata('login')
        );

        $this->db->insert('_collect', $data);
    }
    //supprimer un pokémon de la collection
    public function pokedex_del_pokemon($pokemon_id){
        $data = array('pokemon_id' => $pokemon_id, 'collector_login' => $this->session->userdata('login')
        );

        $this->db->delete('_collect', $data);
    }
}
?>
<?php
class Collection extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('pokemon_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        //Charge la vue "Collection"
        $data['content']='Collection';
        $data['collect']=$this->pokemon_model->pokedex_get_collect();
        $this->load->vars($data);
        $this->load->view('template');
    }
    
    //supprimer un pokémon de la collection
    public function del_collect(){
        $data['pokedex']=$this->pokemon_model->pokedex_del_pokemon($_GET['id']);
        $this->load->vars($data);
        //Charge la vue "Collection" //Remplacer par un appel à collection.index() ?
        $data['content']='Collection';
        $data['collect']=$this->pokemon_model->pokedex_get_collect();
        $this->load->vars($data);
        $this->load->view('template');
    }
}
?>
<?php
class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('pokemon_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        //Charge la vue "Connexion"
        $data['content']='Connexion';
        $this->load->vars($data);
        $this->load->view('template');
    }
    //Vérification que la connexion est ok
    public function connexion_valide(){
        $verif=array(
                'required'      => '%s : Veuillez compléter le champs.');
        //Défini les règles de saisie
        $this->form_validation->set_rules('login','Login','required',$verif);
        $this->form_validation->set_rules('password','Password','required',$verif);
        
        if($this->form_validation->run()!==FALSE){ //Si les règles sont correctes
            $login=$this->input->post('login');
            $password=$this->input->post('password');
            
            $user=$this->user_model->get_user($login);
            
            if($this->user_model->user_login($login,$password)){
                $_SESSION['login']        = (string)$user->login;
                $_SESSION['logged_in']    = (bool)true;
                
                //Charge la vue "Accueil"
                $data['content']='Accueil';
                if(isset($_SESSION['login']) AND $_SESSION['login']!='admin'){ //User connecté & non admin (admin n'a pas de collection)
                    $data['collect']=$this->pokemon_model->pokedex_get_collect_mini(); //Affiche la collection en "mini"
                }
                $query=$this->db->select('_pokemon.pokemon_id')->order_by('pokemon_id', 'desc')->limit(1)->get('_pokemon');
                $ret = $query->row_array();
                $data['nb']=$ret['pokemon_id'];
                $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon();
                $this->load->vars($data);
                $this->load->view('template');
            }else{
                //Charge la vue "Connexion"
                $data['content']='Connexion';
                $this->load->vars($data);
                $this->load->view('template');
            }
        }else{
            //Charge la vue "Connexion"
            $data['content']='Connexion';
            $this->load->vars($data);
            $this->load->view('template');
        }
    }
}
?>
<?php
class Signup extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('pokemon_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        //Charge la vue "Inscription"
        $data['content']='Inscription';
        $this->load->vars($data);
        $this->load->view('template');
    }
    //load la page de validaton d'inscription, en chargeant le timer de 5sec et en vérifiant que tout est valide
    public function inscription_valide(){
        $verif=array( //Défini les messages d'erreurs
                'required'      => '%s : Veuillez compléter le champs.',
                'alpha'         => '%s : Seuls les lettres sont autorisés.',
                'alpha_numeric' => '%s : Les caractères spéciaux ne sont pas autorisés.',
                'min_length'    => '%s : Minimum 3 caractères.',
                'max_length'    => '%s : Nombre de caractères limités.',
                'is_unique'     => '%s : Déjà utilisé.',
                'matches'       => 'Les mots de passes ne correspondent pas.');
        
        //Défini les règles de saisie
        $this->form_validation->set_rules('login','Identifiant','trim|required|alpha_numeric|min_length[3]|max_length[8]|is_unique[_collector.login]',$verif);
        $this->form_validation->set_rules('name','Prénom','trim|required|alpha|min_length[3]|max_length[50]',$verif);
        $this->form_validation->set_rules('firstname','Nom','trim|required|alpha|min_length[3]|max_length[50]',$verif);
        $this->form_validation->set_rules('password','Mot de passe','trim|required|alpha_numeric|min_length[3]|max_length[20]',$verif);
        $this->form_validation->set_rules('passconf','Confirmation de mot de passe','required|matches[password]',$verif);
        
        if($this->form_validation->run()!==FALSE){ //Si les règles sont respectés
            $login=$this->input->post('login');
            $name=$this->input->post('name');
            $firstname=$this->input->post('firstname');
            $password=$this->input->post('password');
            $this->user_model->user_signup($login,$name,$firstname,$password);
            
            //Charge la vue "Signup" -> Message de confirmation d'inscription / Avant un retour à l'accueil
            $data['content']='Signup';
            $this->load->vars($data);
            $this->load->view('template');
        }else{
            //Charge la vue "Inscription"
            $data['content']='Inscription';
            $this->load->vars($data);
            $this->load->view('template');
        }
    }
}
?>


<?php
class Administration extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        //Charge la vue "Administration"
        $data['content']='Administration';
        $data['users']=$this->admin_model->users_get_collector();
        $this->load->vars($data);
        $this->load->view('template');
    }
    
    //Bannir un utilisateur
    public function del_user(){ 
        if($_GET['login']!='admin'){ //L'admin ne peut être ban
            $data['pokedex']=$this->admin_model->del_user($_GET['login']);
            $this->load->vars($data);
        }
        $data['content']='Administration';
        $data['users']=$this->admin_model->users_get_collector();
        $this->load->vars($data);
        $this->load->view('template');
    }
}
?>
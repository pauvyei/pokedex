<?php
class About extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }
    public function index(){
        //Charge la vue "à propos"
        $data['content']='About';
        $this->load->vars($data);
        $this->load->view('template');
    }
}
?>
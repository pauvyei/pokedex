<?php
class Logout extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('pokemon_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) { //Si un user est connecté
			//Supprime les données de session
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
        }
        //Charge la vue "Accueil"
        $data['content']='Accueil';
        $query=$this->db->select('_pokemon.pokemon_id')->order_by('pokemon_id', 'desc')->limit(1)->get('_pokemon');
        $ret = $query->row_array();
        $data['nb']=$ret['pokemon_id'];
        $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon();
        $this->load->vars($data);
        $this->load->view('template');
    }
    
}
?>
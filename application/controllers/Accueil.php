<?php
class Accueil extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('pokemon_model');
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        //Charge la vue "Accueil"
        $data['content']='Accueil';
        $query=$this->db->select('_pokemon.pokemon_id')->order_by('pokemon_id', 'desc')->limit(1)->get('_pokemon');
        $ret = $query->row_array();
        $data['nb']=$ret['pokemon_id'];
        $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon();
        $data['collect']=$this->pokemon_model->pokedex_get_collect_mini();
        $this->load->vars($data);
        $this->load->view('template');
    }
    
    //ajout à la collection depuis la page d'accueil de base
    public function add_collect(){
        if($_GET['id']>=1 AND $_GET['id']<=807){ //Les pokémons dont l'id est < à 1 ou > 807 ne sont pas ajoutable
            $count = 0;
            $collect = $this->pokemon_model->pokedex_get_collect();
            $present = false;
            
            foreach($collect as $pokemon){
                $count = $count + 1;
                if($pokemon['pokemon_id']==$_GET['id']){
                    $present = true;
                }
            }
            if(isset($_SESSION['login']) AND $_SESSION['login']!='admin'){
                if($present == false){
                    if($count<10){
                        $data['pokedex']=$this->pokemon_model->pokedex_add_pokemon($_GET['id']);
                        $this->load->vars($data);
                    }
                    else{
                        $query=$this->db->select('_collect.pokemon_id')->join('_pokemon', '_pokemon.pokemon_id = _collect.pokemon_id')->order_by('base_experience', 'asc')->limit(1)->get_where('_collect', array('collector_login' => $this->session->userdata('login')));
                        $ret = $query->row_array();
                        $data['pokedex']=$this->pokemon_model->pokedex_del_pokemon($ret['pokemon_id']);
                        $data['pokedex']=$this->pokemon_model->pokedex_add_pokemon($_GET['id']);
                        $this->load->vars($data);
                    }
                }
            }
        }
        $data['content']='Accueil';
        $query=$this->db->select('_pokemon.pokemon_id')->order_by('pokemon_id', 'desc')->limit(1)->get('_pokemon');
        $ret = $query->row_array();
        $data['nb']=$ret['pokemon_id'];
       if(!isset($_GET['filtre'])){
            $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon();
        }
        else if($_GET['filtre']=='identifier'){
            $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon_identifier();
        }
        else if($_GET['filtre']=='height'){
            $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon_height();
        }
        else if($_GET['filtre']=='weight'){
            $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon_weight();
        }
        else if($_GET['filtre']=='experience'){
            $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon_experience();
        }
        else{
             $data['pokedex']=$this->pokemon_model->pokedex_get_pokemon();
        }
        $data['collect']=$this->pokemon_model->pokedex_get_collect_mini();
        $this->load->vars($data);
        $this->load->view('template');
        
        $this->load->vars($data);
    }
}

?>
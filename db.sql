--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-10-22 18:53:45 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 12 (class 2615 OID 41228)
-- Name: pokedex; Type: SCHEMA; Schema: -; Owner: -
--


drop schema if exists pokedex cascade;
create schema pokedex;
set schema 'pokedex';


SET search_path = pokedex, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 214 (class 1259 OID 41244)
-- Name: _collect; Type: TABLE; Schema: pokedex; Owner: -
--

CREATE TABLE _collect (
    pokemon_id integer NOT NULL,
    collector_login character(8) NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 41234)
-- Name: _collector; Type: TABLE; Schema: pokedex; Owner: -
--

CREATE TABLE _collector (
    login character varying(8) NOT NULL,
    name character varying(50) NOT NULL,
    firstname character varying(50) NOT NULL,
    password character varying(20) NOT NULL
);


--
-- TOC entry 213 (class 1259 OID 41239)
-- Name: _pokemon; Type: TABLE; Schema: pokedex; Owner: -
--

CREATE TABLE _pokemon (
    pokemon_id integer NOT NULL,
    identifier character varying(50) NOT NULL,
    height integer NOT NULL,
    weight integer NOT NULL,
    base_experience integer NOT NULL
);


--
-- TOC entry 2472 (class 0 OID 41244)
-- Dependencies: 214
-- Data for Name: _collect; Type: TABLE DATA; Schema: pokedex; Owner: -
--



--
-- TOC entry 2470 (class 0 OID 41234)
-- Dependencies: 212
-- Data for Name: _collector; Type: TABLE DATA; Schema: pokedex; Owner: -
--



--
-- TOC entry 2471 (class 0 OID 41239)
-- Dependencies: 213
-- Data for Name: _pokemon; Type: TABLE DATA; Schema: pokedex; Owner: -
--

INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (001,'Bulbizarre',7,69,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (002,'Herbizarre',10,130,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (003,'Florizarre',20,1000,236);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (004,'Salamèche',6,85,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (005,'Reptincel',11,190,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (006,'Dracaufeu',17,905,240);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (007,'Carapuce',5,90,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (008,'Carabaffe',10,225,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (009,'Tortank',16,855,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (010,'Chenipan',3,29,39);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (011,'Chrysacier',7,99,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (012,'Papilusion',11,320,178);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (013,'Aspicot',3,32,39);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (014,'Coconfort',6,100,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (015,'Dardargnan',10,295,178);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (016,'Roucool',3,18,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (017,'Roucoups',11,300,122);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (018,'Roucarnage',15,395,216);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (019,'Rattata',3,35,51);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (020,'Rattatac',7,185,145);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (021,'Piafabec',3,20,52);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (022,'Rapasdepic',12,380,155);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (023,'Abo',20,69,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (024,'Arbok',35,650,157);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (025,'Pikachu',4,60,112);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (026,'Raichu',8,300,218);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (027,'Sabelette',6,120,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (028,'Sablaireau',10,295,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (029,'Nidoran♀',4,70,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (030,'Nidorina',8,200,128);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (031,'Nidoqueen',13,600,227);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (032,'Nidoran♂',5,90,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (033,'Nidorino',9,195,128);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (034,'Nidoking',14,620,227);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (035,'Mélofée',6,75,113);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (036,'Mélodelfe',13,400,217);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (037,'Goupix',6,99,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (038,'Feunard',11,199,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (039,'Rondoudou',5,55,95);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (040,'Groudoudou',10,120,196);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (041,'Nosferapti',8,75,49);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (042,'Nosferalto',16,550,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (043,'Mystherbe',5,54,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (044,'Ortide',8,86,138);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (045,'Rafflesia',12,186,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (046,'Paras',3,54,57);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (047,'Parasect',10,295,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (048,'Mimitoss',10,300,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (049,'Aéromith',15,125,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (050,'Taupiqueur',2,8,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (051,'Triopikeur',7,333,149);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (052,'Miaouss',4,42,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (053,'Persian',10,320,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (054,'Psykokwak',8,196,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (055,'Akwakwak',17,766,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (056,'Férosinge',5,280,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (057,'Colossinge',10,320,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (058,'Caninos',7,190,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (059,'Arcanin',19,1550,194);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (060,'Ptitard',6,124,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (061,'Têtarte',10,200,135);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (062,'Tartard',13,540,230);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (063,'Abra',9,195,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (064,'Kadabra',13,565,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (065,'Alakazam',15,480,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (066,'Machoc',8,195,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (067,'Machopeur',15,705,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (068,'Mackogneur',16,1300,227);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (069,'Chétiflor',7,40,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (070,'Boustiflor',10,64,137);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (071,'Empiflor',17,155,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (072,'Tentacool',9,455,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (073,'Tentacruel',16,550,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (074,'Racaillou',4,200,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (075,'Gravalanch',10,1050,137);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (076,'Grolem',14,3000,223);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (077,'Ponyta',10,300,82);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (078,'Galopa',17,950,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (079,'Ramoloss',12,360,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (080,'Flagadoss',16,785,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (081,'Magnéti',3,60,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (082,'Magnéton',10,600,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (083,'Canarticho',8,150,132);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (084,'Doduo',14,392,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (085,'Dodrio',18,852,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (086,'Otaria',11,900,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (087,'Lamantine',17,1200,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (088,'Tadmorv',9,300,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (089,'Grotadmorv',12,300,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (090,'Kokiyas',3,40,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (091,'Crustabri',15,1325,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (092,'Fantominus',13,1,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (093,'Spectrum',16,1,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (094,'Ectoplasma',15,405,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (095,'Onix',88,2100,77);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (096,'Soporifik',10,324,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (097,'Hypnomade',16,756,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (098,'Krabby',4,65,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (099,'Krabboss',13,600,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (100,'Voltorbe',5,104,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (101,'Electrode',12,666,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (102,'Noeunoeuf',4,25,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (103,'Noadkoko',20,1200,186);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (104,'Osselait',4,65,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (105,'Ossatueur',10,450,149);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (106,'Kicklee',15,498,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (107,'Tygnon',14,502,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (108,'Excelangue',12,655,77);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (109,'Smogo',6,10,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (110,'Smogogo',12,95,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (111,'Rhinocorne',10,1150,69);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (112,'Rhinoféros',19,1200,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (113,'Leveinard',11,346,395);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (114,'Saquedeneu',10,350,87);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (115,'Kangourex',22,800,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (116,'Hypotrempe',4,80,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (117,'Hypocéan',12,250,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (118,'Poissirène',6,150,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (119,'Poissoroy',13,390,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (120,'Stari',8,345,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (121,'Staross',11,800,182);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (122,'M.Mime',13,545,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (123,'Insécateur',15,560,100);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (124,'Lippoutou',14,406,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (125,'Élektek',11,300,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (126,'Magmar',13,445,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (127,'Scarabrute',15,550,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (128,'Tauros',14,884,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (129,'Magicarpe',9,100,40);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (130,'Léviator',65,2350,189);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (131,'Lokhlass',25,2200,187);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (132,'Métamorph',3,40,101);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (133,'Évoli',3,65,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (134,'Aquali',10,290,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (135,'Voltali',8,245,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (136,'Pyroli',9,250,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (137,'Porygon',8,365,79);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (138,'Amonita',4,75,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (139,'Amonitar',10,350,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (140,'Kabuto',5,115,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (141,'Kabutops',13,405,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (142,'Ptéra',18,590,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (143,'Ronflex',21,4600,189);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (144,'Artikodin',17,554,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (145,'Électhor',16,526,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (146,'Sulfura 	',20,600,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (147,'Minidraco',18,33,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (148,'Draco',40,165,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (149,'Dracolosse',22,2100,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (150,'Mewtwo',20,1220,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (151,'Mew',4,40,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (152,'Germignon',9,64,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (153,'Macronium',12,158,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (154,'Méganium',18,1005,236);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (155,'Héricendre',5,79,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (156,'Feurisson',9,190,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (157,'Typhlosion',17,795,240);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (158,'Kaiminus',6,95,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (159,'Crocrodil',11,250,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (160,'Aligatueur',23,888,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (161,'Fouinette',8,60,43);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (162,'Fouinar',18,325,145);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (163,'Hoothoot',7,212,52);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (164,'Noarfang',16,408,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (165,'Coxy',10,108,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (166,'Coxyclaque',14,356,137);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (167,'Mimigal',5,85,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (168,'Migalos',11,335,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (169,'Nostenfer',18,750,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (170,'Loupio',5,120,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (171,'Lanturn',12,225,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (172,'Pichu',3,20,41);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (173,'Mélo',3,30,44);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (174,'Toudoudou',3,10,42);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (175,'Togepi',3,15,49);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (176,'Togetic',6,32,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (177,'Natu',2,20,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (178,'Xatu',15,150,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (179,'Wattouat',6,78,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (180,'Lainergie',8,133,128);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (181,'Pharamp',14,615,230);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (182,'Joliflor',4,58,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (183,'Marill',4,85,88);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (184,'Azumarill',8,285,189);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (185,'Simularbre',12,380,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (186,'Tarpaud',11,339,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (187,'Granivol',4,5,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (188,'Floravol',6,10,119);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (189,'Cotovol',8,30,207);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (190,'Capumain',8,115,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (191,'Tournegrin',3,18,36);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (192,'Héliatronc',8,85,149);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (193,'Yanma',12,380,78);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (194,'Axoloto',4,85,42);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (195,'Maraiste',14,750,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (196,'Mentali',9,265,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (197,'Noctali',10,270,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (198,'Cornèbre',5,21,81);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (199,'Roigada',20,795,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (200,'Feuforêve',7,10,87);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (201,'Zarbi',5,50,118);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (202,'Qulbutoké',13,285,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (203,'Girafarig',15,415,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (204,'Pomdepik',6,72,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (205,'Foretress',12,1258,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (206,'Insolourdo',15,140,145);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (207,'Scorplane',11,648,86);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (208,'Steelix',92,4000,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (209,'Snubbull',6,78,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (210,'Granbull',14,487,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (211,'Qwilfish',5,39,88);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (212,'Cizayox',18,1180,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (213,'Caratroc',6,205,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (214,'Scarhino',15,540,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (215,'Farfuret',9,280,86);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (216,'Teddiursa',6,88,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (217,'Ursaring',18,1258,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (218,'Limagma',7,350,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (219,'Volcaropod',8,550,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (220,'Marcacrin',4,65,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (221,'Cochignon',11,558,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (222,'Corayon',6,50,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (223,'Rémoraid',6,120,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (224,'Octillery',9,285,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (225,'Cadoizo',9,160,116);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (226,'Démanta',21,2200,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (227,'Airmure',17,505,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (228,'Malosse',6,108,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (229,'Démolosse',14,350,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (230,'Hyporoi',18,1520,243);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (231,'Phanpy',5,335,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (232,'Donphan',11,1200,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (233,'Porygon2',6,325,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (234,'Cerfrousse',14,712,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (235,'Queulorior',12,580,88);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (236,'Debugant',7,210,42);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (237,'Kapoera',14,480,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (238,'Lippouti',4,60,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (239,'Élekid',6,235,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (240,'Magby',7,214,73);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (241,'Écrémeuh',12,755,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (242,'Leuphorie',15,468,608);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (243,'Raikou',19,1780,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (244,'Entei',21,1980,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (245,'Suicune',20,1870,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (246,'Embrylex',6,720,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (247,'Ymphect',12,1520,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (248,'Tyranocif',20,2020,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (249,'Lugia',52,2160,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (250,'Ho-Oh',38,1990,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (251,'Celebi',6,50,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (252,'Arcko',5,50,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (253,'Massko',9,216,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (254,'Jungko',17,522,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (255,'Poussifeu',4,25,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (256,'Galifeu',9,195,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (257,'Braségali',19,520,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (258,'Gobou',4,76,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (259,'Flobio',7,280,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (260,'Laggron',15,819,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (261,'Medhyèna',5,136,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (262,'Grahyèna',10,370,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (263,'Zigzaton',4,175,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (264,'Linéon',5,325,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (265,'Chenipotte',3,36,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (266,'Armulys',6,100,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (267,'Charmillon',10,284,178);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (268,'Blindalys',7,115,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (269,'Papinox',12,316,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (270,'Nénupiot',5,26,44);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (271,'Lombre',12,325,119);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (272,'Ludicolo',15,550,216);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (273,'Grainipiot',5,40,44);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (274,'Pifeuil',10,280,119);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (275,'Tengalice',13,596,216);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (276,'Nirondelle',3,23,54);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (277,'Hélédelle',7,198,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (278,'Goélise',6,95,54);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (279,'Békipan',12,280,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (280,'Tarsal',4,66,40);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (281,'Kirlia',8,202,97);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (282,'Gardevoir',16,484,233);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (283,'Arakdo',5,17,54);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (284,'Maskadra',8,36,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (285,'Balignon',4,45,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (286,'Chapignon',12,392,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (287,'Parecool',8,240,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (288,'Vigoroth',14,465,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (289,'Monaflèmit',20,1305,252);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (290,'Ningale',5,55,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (291,'Ninjask',8,120,160);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (292,'Munja',8,12,83);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (293,'Chuchmur',6,163,48);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (294,'Ramboum',10,405,126);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (295,'Brouhabam',15,840,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (296,'Makuhita',10,864,47);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (297,'Hariyama',23,2538,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (298,'Azurill',2,20,38);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (299,'Tarinor',10,970,75);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (300,'Skitty',6,110,52);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (301,'Delcatty',11,326,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (302,'Ténéfix',5,110,133);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (303,'Mysdibule',6,115,133);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (304,'Galekid',4,600,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (305,'Galegon',9,1200,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (306,'Galeking',21,3600,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (307,'Méditikka',6,112,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (308,'Charmina',13,315,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (309,'Dynavolt',6,152,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (310,'Élecsprint',15,402,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (311,'Posipi',4,42,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (312,'Négapi',4,42,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (313,'Muciole',7,177,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (314,'Lumivole',6,177,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (315,'Rosélia',3,20,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (316,'Gloupti',4,103,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (317,'Avaltout',17,800,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (318,'Carvanha',8,208,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (319,'Sharpedo',18,888,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (320,'Wailmer',20,1300,80);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (321,'Wailord',145,3980,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (322,'Chamallot',7,240,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (323,'Camérupt',19,2200,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (324,'Chartor',5,804,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (325,'Spoink',7,306,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (326,'Groret',9,715,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (327,'Spinda',11,50,126);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (328,'Kraknoix',7,150,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (329,'Vibraninf',11,153,119);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (330,'Libégon',20,820,234);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (331,'Cacnea',4,513,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (332,'Cacturne',13,774,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (333,'Tylton',4,12,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (334,'Altaria',11,206,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (335,'Mangriff',13,403,160);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (336,'Séviper',27,525,160);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (337,'Séléroc',10,1680,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (338,'Solaroc',12,1540,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (339,'Barloche',4,19,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (340,'Barbicha',9,236,164);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (341,'Écrapince',6,115,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (342,'Colhomard',11,328,164);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (343,'Balbuto',5,215,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (344,'Kaorine',15,1080,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (345,'Lilia',10,238,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (346,'Vacilys',15,604,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (347,'Anorith',7,125,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (348,'Armaldo',15,682,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (349,'Barpeau',6,74,40);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (350,'Milobellus',62,1620,189);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (351,'Morphéo',3,8,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (352,'Kecleon',10,220,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (353,'Polichombr',6,23,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (354,'Branette',11,125,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (355,'Skelénox',8,150,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (356,'Téraclope',16,306,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (357,'Tropius',20,1000,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (358,'Éoko',6,10,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (359,'Absol',12,470,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (360,'Okéoké',6,140,52);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (361,'Stalgamin',7,168,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (362,'Oniglali',15,2565,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (363,'Obalie',8,395,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (364,'Phogleur',11,876,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (365,'Kaimorse',14,1506,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (366,'Coquiperl',4,525,69);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (367,'Serpang',17,270,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (368,'Rosabyss',18,226,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (369,'Relicanth',10,234,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (370,'Lovdisc',6,87,116);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (371,'Draby',6,421,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (372,'Drackhaus',11,1105,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (373,'Drattak',15,1026,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (374,'Terhall',6,952,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (375,'Métang',12,2025,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (376,'Métalosse',16,5500,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (377,'Regirock',17,2300,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (378,'Regice',18,1750,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (379,'Registeel',19,2050,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (380,'Latias',14,400,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (381,'Latios',20,600,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (382,'Kyogre',45,3520,302);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (383,'Groudon',35,9500,302);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (384,'Rayquaza',70,2065,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (385,'Jirachi',3,11,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (386,'Deoxys',17,608,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (387,'Tortipouss',4,102,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (388,'Boskara',11,970,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (389,'Torterra',22,3100,236);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (390,'Ouisticram',5,62,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (391,'Chimpenfeu',9,220,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (392,'Simiabraz',12,550,240);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (393,'Tiplouf',4,52,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (394,'Prinplouf',8,230,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (395,'Pingoléon',17,845,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (396,'Étourmi',3,20,49);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (397,'Étourvol',6,155,119);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (398,'Étouraptor',12,249,218);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (399,'Keunotor',5,200,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (400,'Castorno',10,315,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (401,'Crikzik',3,22,39);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (402,'Mélokrik',10,255,134);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (403,'Lixy',5,95,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (404,'Luxio',9,305,127);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (405,'Luxray',14,420,235);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (406,'Rozbouton',2,12,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (407,'Roserade',9,145,232);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (408,'Kranidos',9,315,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (409,'Charkos',16,1025,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (410,'Dinoclier',5,570,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (411,'Bastiodon',13,1495,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (412,'Cheniti',2,34,45);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (413,'Cheniselle',5,65,148);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (414,'Papilord',9,233,148);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (415,'Apitrini',3,55,49);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (416,'Apireine',12,385,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (417,'Pachirisu',4,39,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (418,'Mustébouée',7,295,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (419,'Mustéflott',11,335,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (420,'Ceribou',4,33,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (421,'Ceriflor',5,93,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (422,'Sancoki',3,63,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (423,'Tritosor',9,299,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (424,'Capidextre',12,203,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (425,'Baudrive',4,12,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (426,'Grodrive',12,150,174);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (427,'Laporeille',4,55,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (428,'Lockpin',12,333,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (429,'Magirêve',9,44,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (430,'Corboss',9,273,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (431,'Chaglam',5,39,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (432,'Chaffreux',10,438,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (433,'Korillon',2,6,57);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (434,'Moufouette',4,192,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (435,'Moufflair',10,380,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (436,'Archéomire',5,605,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (437,'Archéodong',13,1870,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (438,'Manzaï',5,150,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (439,'Mime Jr.',6,130,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (440,'Ptiravi',6,244,110);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (441,'Pijako',5,19,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (442,'Spiritomb',10,1080,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (443,'Griknot',7,205,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (444,'Carmache',14,560,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (445,'Carchacrock',19,950,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (446,'Goinfrex',6,1050,78);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (447,'Riolu',7,202,57);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (448,'Lucario',12,540,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (449,'Hippopotas',8,495,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (450,'Hippodocus',20,3000,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (451,'Rapion',8,120,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (452,'Drascore',13,615,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (453,'Cradopaud',7,230,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (454,'Coatox',13,444,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (455,'Vortente',14,270,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (456,'Écayon',4,70,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (457,'Luminéon',12,240,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (458,'Babimanta',10,650,69);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (459,'Blizzi',10,505,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (460,'Blizzaroi',22,1355,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (461,'Dimoret',11,340,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (462,'Magnézone',12,1800,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (463,'Coudlangue',17,1400,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (464,'Rhinastoc',24,2828,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (465,'Bouldeneu',20,1286,187);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (466,'Élekable',18,1386,243);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (467,'Maganon',16,680,243);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (468,'Togekiss',15,380,245);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (469,'Yanmega',19,515,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (470,'Phyllali',10,255,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (471,'Givrali',8,259,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (472,'Scorvol',20,425,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (473,'Mammochon',25,2910,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (474,'Porygon-Z',9,340,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (475,'Gallame',16,520,233);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (476,'Tarinorme',14,3400,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (477,'Noctunoir',22,1066,236);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (478,'Momartik',13,266,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (479,'Motisma',3,3,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (480,'Créhelf',3,3,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (481,'Créfollet',3,3,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (482,'Créfadet',3,3,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (483,'Dialga',54,6830,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (484,'Palkia',42,3360,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (485,'Heatran',17,4300,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (486,'Regigigas',37,4200,302);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (487,'Giratina',45,7500,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (488,'Cresselia',15,856,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (489,'Phione',4,31,216);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (490,'Manaphy',3,14,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (491,'Darkrai',15,505,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (492,'Shaymin',2,21,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (493,'Arceus',32,3200,324);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (494,'Victini',4,40,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (495,'Vipélierre',6,81,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (496,'Lianaja',8,160,145);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (497,'Majaspic',33,630,238);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (498,'Gruikui',5,99,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (499,'Grotichon',10,555,146);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (500,'Roitiflam',16,1500,238);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (501,'Moustillon',5,59,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (502,'Mateloutre',8,245,145);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (503,'Clamiral',15,946,238);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (504,'Ratentif',5,116,51);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (505,'Miradar',11,270,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (506,'Ponchiot',4,41,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (507,'Ponchien',9,147,130);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (508,'Mastouffe',12,610,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (509,'Chacripan',4,101,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (510,'Léopardus',11,375,156);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (511,'Feuillajou',6,105,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (512,'Feuiloutan',11,305,174);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (513,'Flamajou',6,110,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (514,'Flamoutan',10,280,174);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (515,'Flotajou',6,135,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (516,'Flotoutan',10,290,174);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (517,'Munna',6,233,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (518,'Mushana',11,605,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (519,'Poichigeon',3,21,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (520,'Colombeau',6,150,125);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (521,'Déflaisan',12,290,220);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (522,'Zébibron',8,298,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (523,'Zéblitz',16,795,174);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (524,'Nodulithe',4,180,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (525,'Géolithe',9,1020,137);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (526,'Gigalithe',17,2600,232);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (527,'Chovsourir',4,21,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (528,'Rhinolove',9,105,149);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (529,'Rototaupe',3,85,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (530,'Minotaupe',7,404,178);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (531,'Nanméouïe',11,310,390);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (532,'Charpenti',6,125,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (533,'Ouvrifier',12,400,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (534,'Bétochef',14,870,227);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (535,'Tritonde',5,45,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (536,'Batracné',8,170,134);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (537,'Crapustule',15,620,229);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (538,'Judokrak',13,555,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (539,'Karaclée',14,510,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (540,'Larveyette',3,25,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (541,'Couverdure',5,73,133);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (542,'Manternel',12,205,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (543,'Venipatte',4,53,52);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (544,'Scobolide',12,585,126);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (545,'Brutapode',25,2005,218);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (546,'Doudouvet',3,6,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (547,'Farfaduvet',7,66,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (548,'Chlorobule',5,66,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (549,'Fragilady',11,163,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (550,'Bargantua',10,180,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (551,'Mascaïman',7,152,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (552,'Escroco',10,334,123);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (553,'Crocorible',15,963,234);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (554,'Darumarond',6,375,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (555,'Darumacho',13,929,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (556,'Maracachi',10,280,161);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (557,'Crabicoque',3,145,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (558,'Crabaraque',14,2000,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (559,'Baggiguane',6,118,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (560,'Baggaïd',11,300,171);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (561,'Cryptéro',14,140,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (562,'Tutafeh',5,15,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (563,'Tutankafer',17,765,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (564,'Carapagos',7,165,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (565,'Mégapagos',12,810,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (566,'Arkéapti',5,95,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (567,'Aéroptéryx',14,320,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (568,'Miamiasme',6,310,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (569,'Miasmax',19,1073,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (570,'Zorua',7,125,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (571,'Zoroark',16,811,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (572,'Chinchidou',4,58,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (573,'Pashmilla',5,75,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (574,'Scrutella',4,58,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (575,'Mesmérella',7,180,137);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (576,'Sidérella',15,440,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (577,'Nucléos',3,10,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (578,'Méios',6,80,130);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (579,'Symbios',10,201,221);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (580,'Couaneton',5,55,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (581,'Lakmécygne',13,242,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (582,'Sorbébé',4,57,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (583,'Sorboul',11,410,138);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (584,'Sorbouboul',13,575,241);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (585,'Vivaldaim',6,195,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (586,'Haydaim',19,925,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (587,'Emolga',4,50,150);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (588,'Carabing',5,59,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (589,'Lançargot',10,330,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (590,'Trompignon',2,10,59);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (591,'Gaulet',6,105,162);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (592,'Viskuse',12,330,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (593,'Moyade',22,1350,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (594,'Mamanbo',12,316,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (595,'Statitik',1,6,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (596,'Mygavolt',8,143,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (597,'Grindur',6,188,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (598,'Noacier',10,1100,171);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (599,'Tic',3,210,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (600,'Clic',6,510,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (601,'Cliticlic',6,810,234);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (602,'Anchwatt',2,3,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (603,'Lampéroie',12,220,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (604,'Ohmassacre',21,805,232);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (605,'Lewsor',5,90,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (606,'Neitram',10,345,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (607,'Funécire',3,31,55);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (608,'Mélancolux',6,130,130);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (609,'Lugulabre',10,343,234);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (610,'Coupenotte',6,180,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (611,'Incisache',10,360,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (612,'Tranchodon',18,1055,243);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (613,'Polarhume',5,85,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (614,'Polagriffe',26,2600,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (615,'Hexagel',11,1480,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (616,'Escargaume',4,77,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (617,'Limaspeed',8,253,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (618,'Limonde',7,110,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (619,'Kungfouine',9,200,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (620,'Shaofouine',14,355,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (621,'Drakkarmin',16,1390,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (622,'Gringolem',10,920,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (623,'Golemastoc',28,3300,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (624,'Sclapion',5,102,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (625,'Scalproie',16,700,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (626,'Frison',16,946,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (627,'Furaiglon',5,105,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (628,'Gueriaigle',15,410,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (629,'Vostourno',5,90,74);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (630,'Vaututrice',12,395,179);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (631,'Aflamanoir',14,580,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (632,'Fermite',3,330,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (633,'Solochi',8,173,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (634,'Diamat',14,500,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (635,'Trioxhydre',18,1600,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (636,'Pyronille',11,288,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (637,'Pyrax',16,460,248);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (638,'Cobaltium',21,2500,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (639,'Terrakium',19,2600,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (640,'Viridium',20,2000,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (641,'Boréas',15,630,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (642,'Fulguris',15,610,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (643,'Reshiram',32,3300,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (644,'Zekrom',29,3450,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (645,'Démétéros',15,680,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (646,'Kyruem',30,3250,297);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (647,'Keldeo',14,485,261);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (648,'Meloetta',6,65,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (649,'Genesect',15,825,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (650,'Marisson',4,90,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (651,'Boguérisse',7,290,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (652,'Blindépique',16,900,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (653,'Feunnec',4,94,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (654,'Roussil',10,145,143);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (655,'Goupelin',15,390,240);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (656,'Grenousse',3,70,63);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (657,'Croâporal',6,109,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (658,'Amphinobi',15,400,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (659,'Sapereau',4,50,47);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (660,'Excavarenne',10,424,148);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (661,'Passerouge',3,17,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (662,'Braisillon',7,160,134);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (663,'Flambusard',12,245,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (664,'Lépidonille',3,25,40);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (665,'Pérégrain',3,84,75);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (666,'Prismillon',12,170,185);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (667,'Hélionceau',6,135,74);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (668,'Némélios',15,815,177);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (669,'Flabébé',1,1,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (670,'Floette',2,9,130);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (671,'Florges',11,100,248);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (672,'Cabriolaine',9,310,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (673,'Chevroum',17,910,186);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (674,'Pandespiègle',6,80,70);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (675,'Pandarbare',21,1360,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (676,'Couafarel',12,280,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (677,'Psystigri',3,35,71);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (678,'Mistigrix',6,85,163);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (679,'Monorpale',8,20,65);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (680,'Dimoclès',8,45,157);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (681,'Exagide',17,530,234);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (682,'Fluvetin',2,5,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (683,'Cocotine',8,155,162);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (684,'Sucroquin',4,35,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (685,'Cupcanaille',8,50,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (686,'Sepiatop',4,35,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (687,'Sepiatroce',15,470,169);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (688,'Opermine',5,310,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (689,'Golgopathe',13,960,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (690,'Venalgue',5,73,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (691,'Kravarech',18,815,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (692,'Flingouste',5,83,66);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (693,'Gamblast',13,353,100);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (694,'Galvaran',5,60,58);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (695,'Iguolta',10,210,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (696,'Ptyranidur',8,260,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (697,'Rexillius',25,2700,182);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (698,'Amagara',13,252,72);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (699,'Dragmara',27,2250,104);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (700,'Nymphali',10,235,184);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (701,'Brutalibré',8,215,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (702,'Dedenne',2,22,151);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (703,'Strassie',3,57,100);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (704,'Mucuscule',3,28,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (705,'Colimucus',8,175,158);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (706,'Muplodocus',20,1505,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (707,'Trousselin',2,30,165);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (708,'Brocélôme',4,70,62);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (709,'Desséliande',15,710,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (710,'Pitrouille',4,50,67);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (711,'Banshitrouye',9,125,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (712,'Grelaçon',10,995,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (713,'Séracrawl',20,5050,180);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (714,'Sonistrelle',5,80,49);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (715,'Bruyverne',15,850,187);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (716,'Xerneas',30,2150,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (717,'Yveltal',58,2030,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (718,'Zygarde',50,3050,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (719,'Diancie',7,88,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (720,'Hoopa',5,90,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (721,'Volcanion',17,1950,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (722,'Brindibou',3,15,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (723,'Efflèche',7,160,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (724,'Archéduc',16,366,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (725,'Flamiaou',4,43,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (726,'Matoufeu',7,250,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (727,'Félinferno',18,830,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (728,'Otaquin',4,75,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (729,'Otarlette',6,175,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (730,'Otaria',18,440,239);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (731,'Picassaut',3,12,53);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (732,'Piclairon',6,148,124);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (733,'Bazoucan',11,260,218);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (734,'Manglouton',4,60,51);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (735,'Argouste',7,142,146);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (736,'Larvibule',4,44,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (737,'Chrysapile',5,105,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (738,'Lucanon',15,450,225);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (739,'Crabagarre',6,70,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (740,'Crabominable',17,1800,167);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (741,'Plumeline',6,34,167);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (742,'Bombydou',1,2,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (743,'Rubombelle',2,5,162);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (744,'Rocabot',5,92,56);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (745,'Lougaroc',8,250,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (746,'Froussardine',2,3,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (747,'Vorastérie',4,80,61);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (748,'Prédastérie',7,145,173);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (749,'Tiboudet',10,1100,77);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (750,'Bourrinos',25,9200,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (751,'Araqua',3,40,54);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (752,'Tarenbulle',18,820,159);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (753,'Mimantis',3,15,50);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (754,'Floramantis',9,185,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (755,'Spododo',2,15,57);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (756,'Lampignon',10,115,142);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (757,'Tritox',6,48,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (758,'Malamandre',12,222,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (759,'Nounourson',5,68,68);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (760,'Chelours',21,1350,175);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (761,'Croquine',3,32,42);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (762,'Candine',7,82,102);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (763,'Sucreine',12,214,230);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (764,'Guérilande',1,3,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (765,'Gouroutan',15,760,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (766,'Quatermac',20,828,172);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (767,'Sovkipou',5,120,46);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (768,'Sarmuraï',20,1080,186);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (769,'Bacabouh',5,700,64);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (770,'Trépassable',13,2500,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (771,'Concombaffe',3,12,144);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (772,'Type:0',19,1205,107);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (773,'Silvallié',23,1005,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (774,'Météno',3,400,154);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (775,'Dodoala',4,199,168);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (776,'Boulata',20,2120,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (777,'Togedemaru',3,33,152);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (778,'Mimiqui',2,7,167);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (779,'Denticrisse',9,190,166);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (780,'Draïeul',30,1850,170);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (781,'Sinistrail',39,2100,181);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (782,'Bébécaille',6,297,60);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (783,'Écaïd',12,470,147);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (784,'Ékaïser',16,782,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (785,'Tokorico',18,205,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (786,'Tokopiyon',12,186,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (787,'Tokotoro',19,455,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (788,'Tokopisco',13,212,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (789,'Cosmog',2,1,40);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (790,'Cosmovum',1,9999,140);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (791,'Solgaleo',34,2300,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (792,'Lunala',40,1200,306);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (793,'Zéroïd',12,555,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (794,'Mouscoto',24,3336,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (795,'Cancrelove',18,250,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (796,'Câblifère',38,1000,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (797,'Bamboiselle',92,9999,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (798,'Katagami',3,1,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (799,'Engloutyran',55,8880,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (800,'Necrozma',24,2300,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (801,'Magearna',10,805,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (802,'Marshadow',7,222,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (803,'Vémini',6,18,189);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (804,'Mandrillon',36,1500,243);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (805,'Ama-Ama',55,8200,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (806,'Pierroteknik',18,130,257);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (807,'Zeraora',15,445,270);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (808,'Meltan',2,8,98);
INSERT INTO _pokemon(pokemon_id,identifier,height,weight,base_experience) VALUES (809,'Melmetal',25,800,253);


insert into pokedex._collector values('log1','name1','fname1','pass1');
insert into pokedex._collector values('admin','name2','fname2','pikachu');
--
-- TOC entry 2345 (class 2606 OID 41248)
-- Name: _collect_pkey; Type: CONSTRAINT; Schema: pokedex; Owner: -
--

ALTER TABLE ONLY _collect
    ADD CONSTRAINT _collect_pkey PRIMARY KEY (pokemon_id, collector_login);


--
-- TOC entry 2341 (class 2606 OID 41238)
-- Name: _collector_pkey; Type: CONSTRAINT; Schema: pokedex; Owner: -
--

ALTER TABLE ONLY _collector
    ADD CONSTRAINT _collector_pkey PRIMARY KEY (login);


--
-- TOC entry 2343 (class 2606 OID 41243)
-- Name: _pokemon_pkey; Type: CONSTRAINT; Schema: pokedex; Owner: -
--

ALTER TABLE ONLY _pokemon
    ADD CONSTRAINT _pokemon_pkey PRIMARY KEY (pokemon_id);


--
-- TOC entry 2347 (class 2606 OID 41254)
-- Name: _collect_fk_collector; Type: FK CONSTRAINT; Schema: pokedex; Owner: -
--

ALTER TABLE ONLY _collect
    ADD CONSTRAINT _collect_fk_collector FOREIGN KEY (collector_login) REFERENCES _collector(login);


--
-- TOC entry 2346 (class 2606 OID 41249)
-- Name: _collect_fk_pokemon; Type: FK CONSTRAINT; Schema: pokedex; Owner: -
--

ALTER TABLE ONLY _collect
    ADD CONSTRAINT _collect_fk_pokemon FOREIGN KEY (pokemon_id) REFERENCES _pokemon(pokemon_id);


-- Completed on 2016-10-22 18:53:46 CEST

--
-- PostgreSQL database dump complete
--


# Pokédex

Application web développée lors d'un projet scolaire, en PHP et à l'aide du framework CodeIgniter, suivant un modèle MVC (Modèle-Vue-Contrôleur).

Elle liste, avec une pagination et plusieurs filtres, plus de 800 Pokémons et permet de gérer une collection propre à chaque utilisateur (protégée par compte).